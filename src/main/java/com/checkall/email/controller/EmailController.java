package com.checkall.email.controller;

import com.checkall.email.model.EmailMessage;
import com.checkall.email.service.EmailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/email")
//@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }


    @GetMapping("/sendEmail")
    @ApiOperation(value = "Send email")
    public String sendEmail(@RequestParam("email") String email, @RequestParam("name") String name) {
        try {
            emailService.sendVerificationEmail(email, name);
        } catch (Exception e) {
            return "false";
        }
        return" true";
    }

    @PostMapping("/sendEmailForgotPassword")
    @ApiOperation(value = "Send email")
    public String sendEmailChange(@RequestBody EmailMessage emailMessage) {
        try {
            emailService.sendEmailForgotPassword(emailMessage);
        } catch (Exception e) {
            return "false";
        }
        return" true";
    }

    @GetMapping("/verify")
    @ApiOperation(value = "Verify user with token")
    public String verifyUser(@RequestParam("code") String code) {
        return emailService.verify(code);
    }

}
