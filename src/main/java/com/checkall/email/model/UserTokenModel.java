package com.checkall.email.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@Entity
@Table
public class UserTokenModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank
    private String userEmail;
    private String token;
    private boolean enabled;

    @NotBlank
    @Enumerated(EnumType.STRING)
    private EmailType emailType;

}
