package com.checkall.email.model;

import lombok.Data;

@Data
public class EmailMessage {
    private String email;
    private String name;
    private String otp;
}
