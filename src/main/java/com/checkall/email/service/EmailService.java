package com.checkall.email.service;

import com.checkall.email.model.EmailMessage;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface EmailService {

    void sendVerificationEmail(String email, String name) throws MessagingException, UnsupportedEncodingException;

    String verify(String verificationCode);

    void sendEmailForgotPassword(EmailMessage emailMessage) throws UnsupportedEncodingException, MessagingException;
}
