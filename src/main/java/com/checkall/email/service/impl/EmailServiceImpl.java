package com.checkall.email.service.impl;

import com.checkall.email.model.EmailMessage;
import com.checkall.email.model.EmailType;
import com.checkall.email.model.UserTokenModel;
import com.checkall.email.repository.UserTokenRepository;
import com.checkall.email.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;

@Slf4j
//@AllArgsConstructor
//@RequiredArgsConstructor
//@Transactional
@Service
public class EmailServiceImpl implements EmailService {

    @Value("${siteURL}")
    private String siteURL;

    //application.properties da ya da config class
    private final JavaMailSender mailSender;
    private final UserTokenRepository repository;

    private final String registerContent = "Dear [[name]],<br>"
            + "Please click the link below to verify your registration:<br>"
            + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
            + "Thank you,<br>"
            + "Your company name.";

    private final String forgotPasswordContent = "Dear [[name]],<br>"
            + "Please click the link below to change your password:<br>"
            + "<h3><a href=\"[[URL]]\" target=\"_self\">CHANGE</a></h3>"
            + "Thank you,<br>"
            + "Your company name.";

    private final String changeEmailContent = "Dear [[name]],<br>"
            + "Please click the link below to change your email:<br>"
            + "<h3><a href=\"[[URL]]\" target=\"_self\">CHANGE</a></h3>"
            + "Thank you,<br>"
            + "Your company name.";

    public EmailServiceImpl(JavaMailSender mailSender, UserTokenRepository repository) {
        this.mailSender = mailSender;
        this.repository = repository;
    }


    public void sendVerificationEmail(String email, String name) throws MessagingException, UnsupportedEncodingException {
        UserTokenModel model = new UserTokenModel();
        validateEmail(email);
        MimeMessage message = mailSender.createMimeMessage();

        email = MimeUtility.encodeText(email, "utf-8", "B");
        InternetAddress[] addressArray = InternetAddress.parse(email, false);
        message.addRecipients(MimeMessage.RecipientType.TO, addressArray);
        String code = RandomString.make(64);
        setHelper(name, message, email, code, EmailType.EMAIL_TYPE_REGISTER);
        mailSender.send(message);
        model.setToken(code);
        model.setUserEmail(email);
        model.setEmailType(EmailType.EMAIL_TYPE_REGISTER);
        repository.save(model);
        log.info("Email has been sent");
    }

    private void setHelper(String name, MimeMessage message, String email, String code, EmailType emailType) throws MessagingException, UnsupportedEncodingException {
        String fromAddress = email;

        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom(fromAddress, name);
        helper.setTo(email);

        String verifyURL = "";
        String content = "";
        if (EmailType.EMAIL_TYPE_REGISTER.equals(emailType)) {
            helper.setSubject("Please verify your registration");
            verifyURL = siteURL.concat("/auth/reset-password/").concat(code);
            content = registerContent;
            content = content.replace("[[name]]", name);
            content = content.replace("[[URL]]", verifyURL);
        } else if (EmailType.EMAIL_TYPE_FORGOT_PASSOWORD.equals(emailType)) {
            helper.setSubject("Please check your password reset");
            verifyURL = siteURL.concat("/auth/reset-password/").concat(email+"/").concat(code);
            content = forgotPasswordContent;
            content = content.replace("[[name]]", name);
            content = content.replace("[[URL]]", verifyURL);
        } else if (EmailType.EMAIL_TYPE_CHANGE_EMAIL.equals(emailType)) {
            helper.setSubject("Email change link");
            verifyURL = siteURL.concat("auth/reset-password/").concat(code);
            content = changeEmailContent;
            content = content.replace("[[name]]", name);
            content = content.replace("[[URL]]", verifyURL);
        }


        helper.setText(content, true);
    }

    private boolean validateEmail(String email) {
        boolean isValid = false;
        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            isValid = true;
        } catch (AddressException e) {
            e.printStackTrace();
        }
        return isValid;
    }

    @Transactional
    public String verify(String verificationCode) {
        UserTokenModel model = repository.findByToken(verificationCode);

        if (model == null || model.isEnabled()) {
            return null;
        } else {
            model.setToken(null);
            model.setEnabled(true);
            repository.save(model);
            return model.getUserEmail();
        }

    }

    @Override
    public void sendEmailForgotPassword(EmailMessage emailMessage) throws UnsupportedEncodingException, MessagingException {
        String email = emailMessage.getEmail();
        String name = emailMessage.getName();
        String code = emailMessage.getOtp();
        UserTokenModel model = new UserTokenModel();
        validateEmail(email);
        MimeMessage message = mailSender.createMimeMessage();

        email = MimeUtility.encodeText(email, "utf-8", "B");
        InternetAddress[] addressArray = InternetAddress.parse(email, false);
        message.addRecipients(MimeMessage.RecipientType.TO, addressArray);
//        String code = RandomString.make(64);
        setHelper(name, message, email, code, EmailType.EMAIL_TYPE_FORGOT_PASSOWORD);
        mailSender.send(message);
        model.setToken(code);
        model.setUserEmail(email);
        model.setEmailType(EmailType.EMAIL_TYPE_FORGOT_PASSOWORD);
        repository.save(model);
        log.info("Email has been sent");
    }
}
