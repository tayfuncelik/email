package com.checkall.email.repository;

import com.checkall.email.model.UserTokenModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
@Repository
public interface UserTokenRepository extends JpaRepository<UserTokenModel, Long> {
    //UserTokenModel findByUserEmail(String email);

    UserTokenModel findByToken(String code);
}
